<?php

/**
 * @file
 * ContentProfileFeedsUserProcessor class.
 */

define('CONTENT_PROFILE_FEEDS_BATCH_SIZE', 50);

/**
 * Feeds processor plugin. Create users from feed items.
 * Also, create/update content profile node(s).
 */
class ContentProfileFeedsUserProcessor extends FeedsUserProcessor {

  /**
   * Override parent::process().
   */
  public function process(FeedsImportBatch $batch, FeedsSource $source) {

    // Keep track of processed items in this pass, set total number of items.
    $processed = 0;
    if (!$batch->getTotal(FEEDS_PROCESSING)) {
      $batch->setTotal(FEEDS_PROCESSING, count($batch->items));
    }

    $content_profile_types = content_profile_get_types('names');

    while ($item = $batch->shiftItem()) {

      if (!($uid = $this->existingItemId($batch, $source)) || $this->config['update_existing']) {

        // Map item to a term.
        $account = $this->map($batch);

        // Store content profile values.
        $content_profile_values = array();
        foreach ((array) $account as $key => $value) {
          $matches = array();
          if (preg_match('/cp:(.*):(.*)/', $key, $matches)) {
            // $matches[1] == type_name; $matches[2] == field_name
            $content_profile_values[$matches[1]][$matches[2]] = $value;
            unset($account->key); // user_save() doesn't need this
          }
        }

        // Keep track of taxonomy values
        if (isset($account->taxonomy)) {
          $taxonomy = $account->taxonomy;
          unset($account->taxonomy);
        }

        // Check if user name and mail are set, otherwise continue.
        if (empty($account->name) || empty($account->mail) || !valid_email_address($account->mail)) {
          $processed++;
          continue;
        }

        // Add term id if available.
        if (!empty($uid)) {
          $account->uid = $uid;
        }

        // Save the user.
        user_save($account, (array) $account);
        if ($account->uid && !empty($account->openid)) {
          $authmap = array(
            'uid' => $account->uid,
            'module' => 'openid',
            'authname' => $account->openid,
          );
          if (SAVED_UPDATED != drupal_write_record('authmap', $authmap, array('uid', 'module'))) {
            drupal_write_record('authmap', $authmap);
          }
        }

        if ($uid) {
          $batch->updated++;
        }
        else {
          $batch->created++;
        }

        // Create/update content profile node(s).
        if (!$account->uid) {
          $account = user_load(array('mail' => $account->mail));
        }

        foreach ($content_profile_values as $type_name => $fields) {
          if (array_key_exists($type_name, $content_profile_types) && !empty($fields)) {
            // Try to load existing node.
            $profile_node = content_profile_load($type_name, $account->uid);
            if (empty($profile_node)) {
              $profile_node = array(
                'uid' => $account->uid,
                'name' => isset($account->name) ? $account->name : '',
                'type' => $type_name,
                'language' => '',
              );
              $profile_node = (object) $profile_node;
            }

            // Populate fields values.
            foreach ($fields as $field_name => $value) {
              $profile_node->{$field_name} = $value;
            }

            // Populate taxonomy information
            if (isset($taxonomy)) {
              $profile_node->taxonomy = $taxonomy;
            }

            // "Commit" changes.
            node_save($profile_node);
          }
        }

        $processed++;
        if ($processed >= variable_get('content_profile_feeds_batch_size', CONTENT_PROFILE_FEEDS_BATCH_SIZE)) {
          $batch->setProgress(FEEDS_PROCESSING, $batch->created + $batch->updated);
          return;
        }
      }
    }

    // Set messages.
    if ($batch->created) {
      drupal_set_message(format_plural($created, 'Created @number user.', 'Created @number users.', array('@number' => $batch->created)));
    }
    if ($batch->updated) {
      drupal_set_message(format_plural($updated, 'Updated @number user.', 'Updated @number users.', array('@number' => $batch->updated)));
    }
    else {
      drupal_set_message(t('There are no new users.'));
    }

    $batch->setProgress(FEEDS_PROCESSING, FEEDS_BATCH_COMPLETE);
  }

  /**
   * Override parent::getMappingTargets().
   */
  public function getMappingTargets() {
    $targets = parent::getMappingTargets();

    foreach (content_profile_get_types('names') as $type_name => $label) {
      drupal_alter('feeds_node_processor_targets', $targets, $type_name);

      $type = content_types($type_name);

      // {node}.title
      if (!empty($type['has_title'])) {
        $key = "cp:{$type_name}:title";
        $args = array(
          '@type' => $type['name'],
          '@field' => $type['title_label'],
        );
        $targets[$key] = array(
          'name' => t('@type: @field', $args),
          'description' => t('@type: @field', $args),
          'content_profile_type' => $type_name,
        );
      }

      // Taxonomy
      foreach ($targets as $target_key => $target) {
        if (substr($target_key, 0, 9) == 'taxonomy:') {
          // Rename taxonomy targets
          $args = array(
            '@type' => $type['name'],
            '!prev_name' => $target['name'],
            '!prev_desc' => $target['description'],
          );
          $targets[$target_key]['name'] = t('@type: !prev_name', $args);
          $targets[$target_key]['description'] = t('@type: !prev_desc', $args);
        }
      }

      // CCK fields
      foreach ($type['fields'] as $field) {
        if (isset($targets[$field['field_name']])) {
          // Move from $field_name to custom $key
          $key = "cp:{$type_name}:{$field['field_name']}";

          $args = array(
            '@type' => $type['name'],
            '@field' => $field['widget']['label'],
          );
          $targets[$key] = $targets[$field['field_name']];
          $targets[$key]['name'] = t('@type: @field', $args);
          $targets[$key]['description'] = t('@type: @field', $args);
          $targets[$key]['content_profile_type'] = $type_name;

          unset($targets[$field['field_name']]);
        }
      }
    }

    return $targets;
  }
}
