<?php

/**
 * @file
 * Exports example definition of Feeds Importer.
 */

/**
 * Implements hook_feeds_importer_default().
 */
function content_profile_feeds_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'user_content_profile_import';
  $feeds_importer->config = array(
    'name' => 'User & Content Profile Import',
    'description' => 'Import Users and Content Profile node(s) from CSV file.',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'ContentProfileFeedsUserProcessor',
      'config' => array(
        'roles' => array(
          3 => 0,
          4 => 0,
        ),
        'update_existing' => 0,
        'status' => '1',
        'mappings' => array(
          0 => array(
            'source' => 'mail',
            'target' => 'mail',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'name',
            'target' => 'name',
            'unique' => 1,
          ),
        ),
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
  );
  $export['user_content_profile_import'] = $feeds_importer;

  return $export;
}
